require 'srp/version'
require 'base_attributes'
require 'base_presenter'
require 'srp_view_helper'

module SRP
    def self.included(base)
        base.extend ClassMethods
    end

    module ClassMethods
        def attributes(name)
            class_name = name.to_s.classify.pluralize
            klass = Kernel.const_get(class_name)
            method_name = name.to_sym
            var_name = ('@' + name.to_s).to_sym
            send :define_method, method_name do
                instance_variable_set var_name, klass.new(self)
            end
            
            methods = klass.instance_methods - Object.instance_methods
            methods.each do |method|
                delegate method.to_sym, :to => method_name
            end
        end
    end
end

ActionView::Base.send :include, SRPViewHelper
