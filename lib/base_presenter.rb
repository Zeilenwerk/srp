module SRP
  class BasePresenter
    def initialize(object, template)
      @object = object
      @template = template

      methods = @object.methods - ActiveRecord::Base.instance_methods - self.methods
      (class << self; self; end).class_eval do
        methods.each do |method|
          define_method method do |*args, &block|
            @object.send(method, *args, &block)
          end
        end
      end
    end

    def method_missing(method, *args, &block)
      @object.send(method, *args, &block)
    end
    
    def self.presents(name)
      define_method(name) do
        @object
      end
    end

    def h
      @template
    end
  end
end
