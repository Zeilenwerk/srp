# Policies are special presenters. They should have no side-effects, that means they should only
# query the objects state and not change it. No touching, just looking.
module SRP
  class BaseAttributes
    def initialize(model)
      @model = model
    end
  end
end
