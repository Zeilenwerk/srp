module SRPViewHelper
  def present(object, klass = nil)
    klass ||= "#{object.class}Presenter".constantize
    presenter = klass.new(object, self)
    yield presenter if block_given?
    presenter
  end

  def present_loop(object, klass = nil)
    klass ||= "#{object.first.class}Presenter".constantize
    object.each { |o|
      presenter = klass.new(o, self)
      yield presenter if block_given?
    }
    nil
  end
end
