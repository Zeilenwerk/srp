# SRP

Easily apply Single Responsibility Principles by the use of attributes objects. SRP has got your back and gets you up and running in no time.

## Installation

Add this line to your application's Gemfile:

    gem 'srp'

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install srp

## Usage

#### Create a `attributes` folder in `/app`
#### Add your attribute objects, e.g.


    class InvitationAttributes < SRP::BaseAttributes
      def expired?
        @model.expiration.past?
      end
    end


#### include SRP in your ActiveRecord model and define the attributes to use:


    class Invitation
      attr_accessible :expiration
      include SRP
      attributes :invitation_attributes
    end


#### Enjoy the ability to test the attrbutes without hitting the database!


## Contributing

1. Fork it
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create new Pull Request
